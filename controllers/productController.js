const auth = require("../auth.js")
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");

// [RETRIEVE PRODUCTS]
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

// [RETRIEVE ACTIVE PRODUCTS]
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};

// [RETRIEVE SINGLE PRODUCT]
module.exports.findProduct = (data) => {
	return Product.findById(data).then((result, err) => {
		if(err) {
			return false
		} else {
			return result
		}
	})
}

// [CREATE PRODUCT]
module.exports.createProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Get permission to the admin to access this request"
		} else {
			let newProduct = new Product({
				name: data.body.name,
				description: data.body.description,
				category: data.body.category,
				price: data.body.price,
				stock: data.body.stock
			});

			return newProduct.save().then((result, err) => {
				if(err) {
					return false
				} else {
					return true
				}
			})
		}
	})
}

// [UPDATE PRODUCT]
module.exports.updateProduct = (admin, id, data) => {
	return User.findById(admin).then((result, err) => {
		if(result.isAdmin == false) {
			return "Get permission to the admin to access this request"
		} else {
			const {name, price, stock} = data
			const update = {
				name: name,
				price: price,
				stock: stock
			}
			
			return Product.findByIdAndUpdate(id, update, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	});
};


// [ARCHIVE PRODUCTS]
module.exports.archiveProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Get permission to the admin to access this request"
		} else {
			return Product.findByIdAndUpdate(data, {isActive: false}, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}

// [UNARCHIVE]
module.exports.unarchiveProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Get permission from an admin to access this request"
		} else {
			return Product.findByIdAndUpdate(data, {isActive: true}, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}


// [DELETE PRODUCT]
module.exports.deleteProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Get permissions to the admin to access this request"
		} else {
			return Product.findByIdAndDelete(data).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}