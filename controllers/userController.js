const auth = require("../auth.js")
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");
const bcrypt = require("bcrypt");


// [EMAIL CHECKER]
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false;
		};
	});
};

// |USER REGISTRATION|

module.exports.registerUser = (reqBody) => {
let newUser = new User({
	fullName: reqBody.fullName,
	email : reqBody.email,
	password : bcrypt.hashSync(reqBody.password,10),
	mobileNo : reqBody.mobileNo
});
return newUser.save().then((user,error) => {
	if (error) {
		return false;
	} else {
		return true;
	};
});


};

// [USER'S AUTHENTICATION]
module.exports.login = (data) => {
	const {email, password} = data;
	return User.findOne({email: email}).then((result, err) => {
		if(result == null) {
			return false;
		} else {
			let isSamePassword = bcrypt.compareSync(password, result.password);
			if(isSamePassword == true) {
				return {access: auth.createAccessToken(result)};
			} else {
				return false;
			}
		}
	});
};

// [RETRIEVE USERS]
module.exports.getAllUsers = (data) => {
	return User.findById(data).then((result, error) => {
		if(result.isAdmin == false) {
			return `Get permission to the admin to access this request`
		} else {
			return User.find().then((result, err) => {
				if(err) {
					return false
				} else {
					result.password = "";
					return result;
				}
			})
		};
	});
};


// [RETRIEVE USER INFO]
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


// [SET USER AS ADMIN]
module.exports.makeAdmin = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Get permission to the admin to access this request"
		} else {
			return User.findByIdAndUpdate(data, {isAdmin: true}, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}
