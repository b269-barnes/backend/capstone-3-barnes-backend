const Cart = require("../models/Cart");


module.exports.getCart = () => {
	return Cart.find({}).then(result => {
		return result;
	});
};

module.exports.addToCart = async (userId, productId, quantity) => {
  try {
    const cartItem = await Cart.findOne({ user: userId, product: productId });

    if (cartItem) {
      // If the item is already in the cart, update the quantity
      cartItem.quantity += quantity;
      await cartItem.save();
    } else {
      // If the item is not in the cart, create a new cart item
      const newCartItem = new Cart({
        user: userId,
        product: productId,
        quantity: quantity
      });
      await newCartItem.save();
    }

    return true;
  } catch (error) {
    throw new Error("Failed to add item to cart.");
  }
};

module.exports.removeFromCart = async (userId, cartItemId) => {
  try {
    const cartItem = await Cart.findOne({ _id: cartItemId, user: userId });
    if (!cartItem) {
      throw new Error("Cart item not found.");
    }
    await cartItem.remove();
    return true;
  } catch (error) {
    throw new Error("Failed to remove item from cart.");
  }
};

module.exports.getCartItems = async (userId) => {
  try {
    const cartItems = await Cart.find({ user: userId })
      .populate("product")
      .exec();
    return cartItems;
  } catch (error) {
    throw new Error("Failed to get cart items.");
  }
};

module.exports.clearCart = async (userId) => {
  try {
    await Cart.deleteMany({ user: userId });
    return true;
  } catch (error) {
    throw new Error("Failed to clear cart.");
  }
};
