const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const cartController = require("../controllers/cartController");


router.get("/cart", (req, res) => {
  cartController.getCart().then(resultFromController => res.send(resultFromController));
});

// Add item to cart
router.post("/add-to-cart", auth.verify, (req, res) => {
  verifiedId = auth.decode(req.headers.authorization).id;
  cartController.addToCart(verifiedId, req.body.productId, req.body.quantity)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send({ error: error.message }));
});

// Remove item from cart
router.delete("/remove-from-cart/:id", auth.verify, (req, res) => {
  verifiedId = auth.decode(req.headers.authorization).id;
  cartController.removeFromCart(verifiedId, req.params.id)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => res.status(500).send({ error: error.message }));
});

module.exports = router;
