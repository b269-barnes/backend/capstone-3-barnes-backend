const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
	{
		fullName: {
			type: String,
			required: [true, 'Full Name is required']
		},
		email: {
			type: String,
			required: [true, 'Email address is required']
		},
		password: {
			type: String,
			required: [true, 'Password is required']
		},
		mobileNo : {
			type : String, 
			required : [true, "Mobile No is required"]
		},
		isAdmin: {
			type: Boolean,
			default: false
		}
	}
);

module.exports = mongoose.model("User", userSchema);