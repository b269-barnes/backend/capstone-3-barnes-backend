const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const productRoute = require("./routes/productRoute.js");
const userRoute = require("./routes/userRoute.js");
const orderRoute = require("./routes/orderRoute.js");
const cartRoute = require("./routes/cartRoute.js");



app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Database Connection //

mongoose.connect("mongodb+srv://mjbarnes03:admin123@cluster0.kasjqle.mongodb.net/capstone-2-Barnes?retryWrites=true&w=majority", {
	useNewUrlParser : true,  
	useUnifiedTopology : true
}
);

mongoose.connection.once("open", () => console.log('Now connected to the database!'));

app.use('/users', userRoute);
app.use('/products', productRoute);
app.use('/orders', orderRoute);
app.use('/cart', cartRoute);

app.listen(process.env.PORT || 7000, () => console.log(`Now connected to port ${process.env.PORT || 7000}`));

